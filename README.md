# app-pokemon-service




## URL DEMO

- SITE: https://modyo.icita.site/
- API REST, DOC: https://modyo.icita.site/swagger-ui/index.html


## Technical characteristics

- Live on Instance Digital Ocean Linux centos 
- Deploy in Docker container (check to docker-compose.yml) 
- subdomain by godaddy.com
- Add SSL Certificate with: https://hub.docker.com/r/jrcs/letsencrypt-nginx-proxy-companion 


## Developer contact

- Name: Ricardo Soler
- Contry: México
- [https://www.linkedin.com/in/risolerh/


## proyect objetive

Project created to anybody that want to see my programing style and good practices. Good Vibes =D

