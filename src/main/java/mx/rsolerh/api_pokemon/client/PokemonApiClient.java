package mx.rsolerh.api_pokemon.client;

import java.io.IOException;

import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;
import mx.rsolerh.api_pokemon.configuration.GraphqlAbstractImpl;
import mx.rsolerh.api_pokemon.model.GraphqlBodyRequest;
import mx.rsolerh.api_pokemon.model.client.pokemons.DtoRootDescription;
import mx.rsolerh.api_pokemon.model.client.pokemons.DtoRootDetails;
import mx.rsolerh.api_pokemon.model.client.pokemons.DtoRootPokemon;
import mx.rsolerh.api_pokemon.model.client.pokemons.PokemonDescrDto;
import mx.rsolerh.api_pokemon.model.client.pokemons.PokemonDetailsDto;
import mx.rsolerh.api_pokemon.model.client.pokemons.PokemonListDto;
import mx.rsolerh.api_pokemon.util.GraphqlSchemaReaderUtil;

/**
 * @author rsole
 *
 */
@Slf4j
@Service
public class PokemonApiClient extends GraphqlAbstractImpl implements IPokemonApiClient {
	
	private final static String URL = "https://beta.pokeapi.co/graphql/v1beta";
	
	PokemonApiClient() {
		super(URL);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public DtoRootPokemon getPokemons(int offset, int limit) throws IOException 
	{	   
	    final String query = GraphqlSchemaReaderUtil.getSchemaFromFileName("pokemon_list")
	    	.replace("$offset", Integer.toString(offset))
	    	.replace("$limit", Integer.toString(limit));

	    GraphqlBodyRequest graphQLRequestBody = new GraphqlBodyRequest();
	    graphQLRequestBody.setQuery(query);	    
	    
	    PokemonListDto pokeDto = super.getPost(PokemonListDto.class, graphQLRequestBody);
	    return pokeDto.getData();
	}
	
	/**
	 * {@inheritDoc} 
	 */
	@Override
	public DtoRootDetails getPokemonDetail(int idPokemon) throws IOException 
	{
	    final String query = GraphqlSchemaReaderUtil.getSchemaFromFileName("pokemon_details")
		    	.replace("$idPokemon", Integer.toString(idPokemon));

		    GraphqlBodyRequest graphQLRequestBody = new GraphqlBodyRequest();
		    graphQLRequestBody.setQuery(query);	    
		    
		    PokemonDetailsDto pokeDto = super.getPost(PokemonDetailsDto.class, graphQLRequestBody);
		    log.debug("response object: {}", pokeDto);		    
		    return pokeDto.getData();
	}

	
	/**
	 * {@inheritDoc} 
	 */
	@Override
	public DtoRootDescription getPokemonDescription(int idPokemon) throws IOException 
	{
	    final String query = GraphqlSchemaReaderUtil.getSchemaFromFileName("pokemon_description")
		    	.replace("$idPokemon", Integer.toString(idPokemon));
	    
	    GraphqlBodyRequest graphQLRequestBody = new GraphqlBodyRequest();
	    graphQLRequestBody.setQuery(query);	    
	    
	    PokemonDescrDto pokeDto = super.getPost(PokemonDescrDto.class, graphQLRequestBody);
	    log.debug("response object: {}", pokeDto);	
	    return pokeDto.getData();
	}

}
