package mx.rsolerh.api_pokemon.client;

import java.io.IOException;

import mx.rsolerh.api_pokemon.model.client.pokemons.DtoRootDescription;
import mx.rsolerh.api_pokemon.model.client.pokemons.DtoRootDetails;
import mx.rsolerh.api_pokemon.model.client.pokemons.DtoRootPokemon;

/**
 * @author rsole
 *
 */
public interface IPokemonApiClient {

	/**
	 * Get list pokemons 
	 * 
	 * @param offset
	 * @param limit
	 * @return
	 * @throws IOException
	 */
	DtoRootPokemon getPokemons (int offset, int limit)  throws IOException;
	
	/**
	 * Get pokemon details by id
	 *  
	 * @param idPokemon
	 * @return
	 * @throws IOException
	 */
	DtoRootDetails getPokemonDetail (int idPokemon)  throws IOException;
	
	
	
	/**
	 * Get Pokemon Description by Id
	 * 
	 * @param idPokemon
	 * @return
	 * @throws IOException
	 */
	DtoRootDescription getPokemonDescription (int idPokemon)  throws IOException;
}
