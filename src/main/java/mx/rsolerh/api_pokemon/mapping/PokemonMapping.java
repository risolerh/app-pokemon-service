package mx.rsolerh.api_pokemon.mapping;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import mx.rsolerh.api_pokemon.model.PokemonResponse;
import mx.rsolerh.api_pokemon.model.client.pokemons.DtoPokemonV2Pokemon;

/**
 * @author rsole
 *
 */
public class PokemonMapping {

	
	/**
	 * @param listPokemon
	 * @return
	 */
	public static List<PokemonResponse> toResponse (List<DtoPokemonV2Pokemon> listPokemon) {
		List<PokemonResponse> result = new ArrayList<PokemonResponse>();
		
		for (DtoPokemonV2Pokemon pokemon : listPokemon ) {
			result.add(PokemonMapping.toResponse(pokemon));
		}
		return result;
	}
	
	/**
	 * @param pokemon
	 * @return
	 */
	public static PokemonResponse toResponse(DtoPokemonV2Pokemon pokemon) {
		PokemonResponse item = new PokemonResponse(); 
		item.setId(pokemon.getId());
		item.setName(pokemon.getName());
		item.setWeight(pokemon.getWeight());
		item.setTypes(getTypes(pokemon));
		item.setAbilities(getAbilities(pokemon));
		return item;

	}

	
    /**
     * @return Pokemon types 
     */
    private static String getTypes(DtoPokemonV2Pokemon pokemon) {
    	var types = pokemon.getPokemon_v2_pokemontypes();
    	if (types == null) {
    		return null;
    	}
        return types.stream()
            .map( type -> String.valueOf(type.getPokemon_v2_type().getName()))
            .collect(Collectors.joining(", "));
    }
    
    /**
     * @return Pokemon abilities
     */
    private static String getAbilities(DtoPokemonV2Pokemon pokemon) {
    	var abilities = pokemon.getPokemon_v2_pokemonabilities();
    	if (abilities == null) {
    		return null;
    	}
        return abilities.stream()
            .map( type -> String.valueOf(type.getPokemon_v2_ability().getName()))
            .collect(Collectors.joining(", "));
    }

}
