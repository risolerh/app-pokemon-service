package mx.rsolerh.api_pokemon.configuration;

import java.io.IOException;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

import lombok.extern.slf4j.Slf4j;
import mx.rsolerh.api_pokemon.model.GraphqlBodyRequest;
import reactor.core.publisher.Mono;

/**
 * @author rsole
 *
 */
@Slf4j
@Service
public abstract class GraphqlAbstractImpl {

	/**
	 * URL Graphql Server
	 */
	private String url;
	
	protected GraphqlAbstractImpl(String url){
		this.url = url;
	}
	
	/**
	 * @param <T>
	 * @param clazz
	 * @param graphqlRequestBody
	 * @return
	 * @throws IOException
	 */
	public <T> T getPost(Class<T> clazz, GraphqlBodyRequest graphqlRequestBody) throws IOException {

	    WebClient webClient = WebClient.builder().build();
	    	    
	    return webClient.post()
	        .uri(this.url)
	        .bodyValue(graphqlRequestBody)
	        .retrieve()
	        .onStatus(HttpStatus::isError, clientResponse -> {
	        	log.error("Code error: {}", clientResponse.statusCode() );
	            return Mono.error(new Exception("!!! have error on getPokemons..."));
	        })
	        .bodyToMono(clazz)
	        .block();
	}
}
