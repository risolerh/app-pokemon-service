package mx.rsolerh.api_pokemon.controller;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import mx.rsolerh.api_pokemon.model.PokemonResponse;
import mx.rsolerh.api_pokemon.service.IPokemonService;

/**
 * @author rsole
 *
 */
@Slf4j
@RestController()
@Api("API Pokemons")
@RequestMapping(value="api/pokemon", 
	produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_JSON_VALUE })
public class RestPokemonController {
	
	@Autowired
	private IPokemonService pokemonService;
	
	/**
	 * Get all pokemons 
	 * 
	 * @param offset
	 * @param limit
	 * @return
	 * @throws IOException
	 */
	@GetMapping()
	public ResponseEntity<List<PokemonResponse>> getPokemonList(
			@RequestParam(defaultValue = "0") int offset,
			@RequestParam(defaultValue = "10") int limit ) 
					throws IOException {
		log.debug("get pokemon list...");		
		List<PokemonResponse> response = pokemonService.getPokemons(offset, limit);		
		return ResponseEntity.status(HttpStatus.OK).body(response);
	}
	
	/**
	 * Get detail Pokemon By Id
	 * 
	 * @param idPokemon
	 * @return
	 * @throws IOException
	 */
	@GetMapping("/{idPokemon}")
	public ResponseEntity<List<PokemonResponse>> getPokemonDetailsById(@PathVariable Integer idPokemon) 
			throws IOException {
		log.debug("get detail by pokemon: {}", idPokemon);
		List<PokemonResponse> response = pokemonService.getPokemonDetail(idPokemon);
		log.debug("response; {}", response);
		return ResponseEntity.status(HttpStatus.OK).body(response);

	}

}
