package mx.rsolerh.api_pokemon.controller;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import lombok.extern.slf4j.Slf4j;
import mx.rsolerh.api_pokemon.model.PageableDto;
import mx.rsolerh.api_pokemon.model.PokemonResponse;
import mx.rsolerh.api_pokemon.service.IPokemonService;
import mx.rsolerh.api_pokemon.util.PageableUtil;

/**
 * @author rsole
 *
 */
@Slf4j
@Controller
public class WebPokemonController implements WebMvcConfigurer {
	
	@Autowired
	private IPokemonService pokemonService;

	
	@GetMapping()
	public String showListPokemon (Model model, 
			@RequestParam(defaultValue = "0") Integer currPage, 
			@RequestParam(defaultValue = "10") Integer sizePages) {
		log.debug("Open listPokemon...");
		PageableDto pageable = PageableUtil.create(1150, currPage, sizePages);
		log.debug("pageable: {}", pageable);
		try {
			List<PokemonResponse> listPokemon = pokemonService.getPokemons(pageable.getOffset(), sizePages);
			model.addAttribute("pageable", pageable);
			model.addAttribute("listPokemon", listPokemon);
		} catch (RuntimeException | IOException e) {
			log.error("!! Exception on showListPokemon: {}", e );
			return "error";
		}
		
		return "pokemon";
	}
	
	
	@GetMapping("{idPokemon}")
	public String showPokemonDetail (Model model, @PathVariable Integer idPokemon) {
		log.debug("Open Pokemon details...");
		try {
			List<PokemonResponse> listPokemon = pokemonService.getPokemonDetail(idPokemon);
								
			var pokeSelected = listPokemon.stream().filter(poke -> poke.getId() == idPokemon)
				      .findFirst().get();			
			pokeSelected.setDescription(pokemonService.getPokemonDescription(idPokemon));
			
			model.addAttribute("listPokemon", listPokemon);
			model.addAttribute("pokemonSelected", pokeSelected);
			
		} catch (RuntimeException | IOException e) {
			log.error("!! Exception showPokemonDetail: {}", e );
			return "error";
		}
		
		return "pokemon_details";
	}
	

}
