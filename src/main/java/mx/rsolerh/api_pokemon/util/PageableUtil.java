package mx.rsolerh.api_pokemon.util;

import mx.rsolerh.api_pokemon.model.PageableDto;

/**
 * @author rsole
 *
 */
public class PageableUtil {

	/**
	 * Create paginable
	 * 
	 * @param sizeData
	 * @param pageSelected
	 * @param sizePage
	 * @return
	 */
	public static PageableDto create( final Integer sizeData,
			final Integer pageSelected, final Integer sizePage) {
		
		var totalPages = (int) Math.floor(sizeData/sizePage);
		
		PageableDto pageable = new PageableDto();
		pageable.setCurrentPage(pageSelected);
		pageable.setSize(sizeData);
		pageable.setTotalPages(totalPages);
		pageable.setOffset(pageSelected * sizePage);

		pageable.setIsPrevious(pageSelected == 0);
		pageable.setIsNext(pageSelected >= totalPages);
		
		return pageable;
	}
}
