package mx.rsolerh.api_pokemon.util;

import java.io.IOException;
import java.io.InputStream;

import lombok.extern.slf4j.Slf4j;

/**
 * @author rsole
 *
 */
@Slf4j
public final class GraphqlSchemaReaderUtil {

  public static String getSchemaFromFileName(final String filename) throws IOException {
	  
	var pathfile = "graphql/" + filename + ".graphql";
	
	// this is the path within the jar file
    InputStream input = GraphqlSchemaReaderUtil.class.getResourceAsStream(pathfile);
    if (input == null) {
        // this is how we load file within editor (eg eclipse)
    	log.debug("bucando alternativa de resource...");
        input = GraphqlSchemaReaderUtil.class.getClassLoader().getResourceAsStream(pathfile);
        log.info(input == null ? "....no encontrada" : "encontrado!!!");
    }
    
    return new String(input.readAllBytes());
  }
  
}
