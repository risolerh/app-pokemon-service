package mx.rsolerh.api_pokemon.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;
import mx.rsolerh.api_pokemon.client.IPokemonApiClient;
import mx.rsolerh.api_pokemon.mapping.PokemonMapping;
import mx.rsolerh.api_pokemon.model.PokemonResponse;
import mx.rsolerh.api_pokemon.model.client.pokemons.DtoPokemonV2pokemonspecies;
import mx.rsolerh.api_pokemon.model.client.pokemons.DtoRootDetails;
import mx.rsolerh.api_pokemon.model.client.pokemons.DtoRootPokemon;

/**
 * @author rsole
 *
 */
@Slf4j
@Service
public class PokemonService implements IPokemonService{
	
	
	@Autowired
	private IPokemonApiClient pokemonApiClient;
	

	/**
	 * {@inheritDoc}@
	 */
	@Cacheable("getPokemons")
	public List<PokemonResponse> getPokemons (int offset, int limit)  throws IOException {
		DtoRootPokemon response = pokemonApiClient.getPokemons(offset, limit);		
		return PokemonMapping.toResponse(response.getPokemon_v2_pokemon());
	}
	

	/**
	 * {@inheritDoc}@
	 */
	@Cacheable("getPokemonDetail")
	public List<PokemonResponse> getPokemonDetail (int idPokemon)  throws IOException {
		
		DtoRootDetails response = pokemonApiClient.getPokemonDetail(idPokemon);
		
		List<PokemonResponse> result = new ArrayList<PokemonResponse>();
		
		for (DtoPokemonV2pokemonspecies dto : response.getPokemon_v2_pokemonspecies() ) {
			
			this.deepPokemonsDetails(result, dto);
		}
		
		result.sort(Comparator.comparing(PokemonResponse::getId));
		
		return result;
	}
	
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	@Cacheable("getPokemonDescription")
	public String getPokemonDescription(int idPokemon) {
		var description = new String();
		try {
			var result  =  pokemonApiClient.getPokemonDescription(idPokemon);
			description = result.getPokemon_v2_pokemonspeciesflavortext_by_pk().getFlavor_text();
		} catch (IOException | RuntimeException e) {
			log.error("!!getPokemonDescription error: {}", e );
		}
		return description;
	}

	
	/**
	 * Order Pokemons Detail 
	 * 
	 * @param result
	 * @param dto
	 */
	private void deepPokemonsDetails (List<PokemonResponse> result, DtoPokemonV2pokemonspecies dto) {
		
		if(dto.getPokemon_v2_evolutionchain() != null) {
			
			for( DtoPokemonV2pokemonspecies species : dto.getPokemon_v2_evolutionchain().getPokemon_v2_pokemonspecies() )
			{
				result.addAll(PokemonMapping.toResponse(species.getPokemon_v2_pokemons()));
			}
		}
	}



}
