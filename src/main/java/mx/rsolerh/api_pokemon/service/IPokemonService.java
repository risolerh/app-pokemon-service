package mx.rsolerh.api_pokemon.service;

import java.io.IOException;
import java.util.List;

import mx.rsolerh.api_pokemon.model.PokemonResponse;

/**
 * @author rsole
 *
 */
public interface IPokemonService {
	
	/**
	 * Get list pokemons 
	 * 
	 * @param offset
	 * @param limit
	 * @return
	 * @throws IOException
	 */
	List<PokemonResponse> getPokemons (int offset, int limit)  throws IOException;
	
	/**
	 * Get pokemon details by id
	 *  
	 * @param idPokemon
	 * @return
	 * @throws IOException
	 */
	List<PokemonResponse> getPokemonDetail (int idPokemon)  throws IOException;
	
	
	/**
	 * Get Description Pokemon 
	 * 
	 * @param idPokemon
	 * @return
	 */
	String getPokemonDescription (int idPokemon);

}
