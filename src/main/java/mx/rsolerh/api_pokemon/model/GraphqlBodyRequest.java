package mx.rsolerh.api_pokemon.model;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author rsole
 *
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class GraphqlBodyRequest implements Serializable {
	
	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 4539485328759775762L;
	
	private String query;
	private Object variables;
}
