package mx.rsolerh.api_pokemon.model;

import java.io.Serializable;

import lombok.Data;

/**
 * @author rsole
 *
 */
@Data
public class PokemonApiBaseRequest implements  Serializable {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = -7131092117688382424L;
	
	private int offset;
	private int limit;
}
