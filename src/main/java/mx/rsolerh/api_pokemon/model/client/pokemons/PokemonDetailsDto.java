package mx.rsolerh.api_pokemon.model.client.pokemons;

import java.io.Serializable;

import lombok.Data;

/**
 * @author rsole
 *
 */
@Data
public class PokemonDetailsDto implements Serializable {

    /**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 310727231007994811L;
	
	public DtoRootDetails data;
	
}
