package mx.rsolerh.api_pokemon.model.client.pokemons;

import java.io.Serializable;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author rsole
 *
 */
@NoArgsConstructor
@Data
public class DtoPokemonV2Pokemonability implements Serializable {

    /**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = -1849144152484455432L;
	
	private int id;
	private DtoPokemonV2Ability pokemon_v2_ability;
}

