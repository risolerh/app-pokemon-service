package mx.rsolerh.api_pokemon.model.client.pokemons;

import java.io.Serializable;
import java.util.List;

import lombok.Data;

/**
 * @author rsole
 *
 */
@Data
public class DtoPokemonV2Pokemon implements Serializable {

    /**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1381199065707918656L;
	
	private Integer id;
	private String name;
	private Integer weight;
	
	private List<DtoPokemonV2Pokemontype> pokemon_v2_pokemontypes;
	private List<DtoPokemonV2Pokemonability> pokemon_v2_pokemonabilities;
    
}
