package mx.rsolerh.api_pokemon.model.client.pokemons;

import java.io.Serializable;
import java.util.List;

import lombok.Data;


/**
 * @author rsole
 *
 */
@Data
public class DtoRootDetails implements Serializable {
	
    /**
     * serialVersionUID
	 * 
	 */
	private static final long serialVersionUID = -5900642209888114536L;
	
	
	private List<DtoPokemonV2pokemonspecies> pokemon_v2_pokemonspecies;
	

}
