package mx.rsolerh.api_pokemon.model.client.pokemons;

import java.io.Serializable;

import lombok.Data;

@Data
public class DtoPokemon_v2_especiesflavor implements Serializable {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 8946626117880358687L;
	
	
	private String flavor_text;
}
