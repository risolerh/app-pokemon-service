package mx.rsolerh.api_pokemon.model.client.pokemons;

import java.io.Serializable;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author rsole
 *
 */
@NoArgsConstructor
@Data
public class DtoPokemonV2Pokemontype implements Serializable {
	
    /**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 2332207836292039395L;
	
	private DtoPokemonV2Type pokemon_v2_type;
}

