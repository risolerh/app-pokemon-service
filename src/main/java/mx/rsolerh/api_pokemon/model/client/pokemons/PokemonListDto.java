package mx.rsolerh.api_pokemon.model.client.pokemons;

import java.io.Serializable;

import lombok.Data;

/**
 * @author rsole
 *
 */
@Data
public class PokemonListDto implements Serializable {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = -4431639777276788509L;
	
	public DtoRootPokemon data;
	
}
