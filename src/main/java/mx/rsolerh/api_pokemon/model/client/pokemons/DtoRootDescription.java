package mx.rsolerh.api_pokemon.model.client.pokemons;

import java.io.Serializable;

import lombok.Data;


/**
 * @author rsole
 *
 */
@Data
public class DtoRootDescription implements Serializable {
	
	/**
     * serialVersionUID
	 * 
	 */
	private static final long serialVersionUID = 6891122231618124609L;
	
	
	
	private DtoPokemon_v2_especiesflavor pokemon_v2_pokemonspeciesflavortext_by_pk;
	

}
