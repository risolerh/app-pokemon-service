package mx.rsolerh.api_pokemon.model.client.pokemons;

import java.io.Serializable;

import lombok.Data;

/**
 * @author rsole
 *
 */
@Data
public class PokemonDescrDto implements Serializable {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1046699838048628786L;
	
	public DtoRootDescription data;
	
}
