package mx.rsolerh.api_pokemon.model.client.pokemons;

import java.io.Serializable;
import java.util.List;

import lombok.Data;

/**
 * @author rsole
 *
 */
@Data
public class DtoPokemonV2pokemonspecies implements Serializable {

    /**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = -1466917757570438101L;
	
	
	private String name;
	private Integer id;
	private DtoPokemonV2Evolutionchain pokemon_v2_evolutionchain;
	private List<DtoPokemonV2Pokemon> pokemon_v2_pokemons;
}