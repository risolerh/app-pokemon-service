package mx.rsolerh.api_pokemon.model.client.pokemons;

import java.io.Serializable;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author rsole
 *
 */
@NoArgsConstructor
@Data
public class DtoPokemonV2Type implements Serializable {

    /**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 7691509837290635187L;
	
	private String name;
	private int id;

}
