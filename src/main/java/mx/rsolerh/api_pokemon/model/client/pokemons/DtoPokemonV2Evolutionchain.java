package mx.rsolerh.api_pokemon.model.client.pokemons;

import java.io.Serializable;
import java.util.List;

import lombok.Data;

@Data
public class DtoPokemonV2Evolutionchain implements Serializable {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 3637531908258091800L;
	
	private List<DtoPokemonV2pokemonspecies> pokemon_v2_pokemonspecies;


}
