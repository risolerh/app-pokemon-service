package mx.rsolerh.api_pokemon.model;

import java.io.Serializable;

import lombok.Data;

/**
 * @author rsole
 *
 */
@Data
public class PokemonResponse implements Serializable {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 8390814670938760407L;
	
	private Integer id;
	private String name;
	private Integer weight;	
	private String types;
	private String abilities;
	private String description;

}
