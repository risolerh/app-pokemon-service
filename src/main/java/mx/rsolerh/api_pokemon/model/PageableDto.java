package mx.rsolerh.api_pokemon.model;

import java.io.Serializable;

import lombok.Data;

/**
 * @author rsole
 *
 */
@Data
public class PageableDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8484074554618782941L;
	
	private Integer currentPage;
	private Integer totalPages;
	private Integer size;
	private Integer offset;
	private Boolean isNext;
	private Boolean isPrevious;

}
