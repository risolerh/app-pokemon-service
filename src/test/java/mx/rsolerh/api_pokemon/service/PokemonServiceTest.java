package mx.rsolerh.api_pokemon.service;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.io.IOException;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;

import mx.rsolerh.api_pokemon.client.IPokemonApiClient;
import mx.rsolerh.api_pokemon.model.PokemonResponse;

/**
 * @author rsole
 *
 */

@SpringBootTest
class PokemonServiceTest {

	@Autowired
	private IPokemonService pokemonService;
	
	@SpyBean
	private IPokemonApiClient pokemonApiClient;

	
	/**
	 * Valid result and cache
	 */
	@Test
	void testGetPokemons() {		
		try {
			
			List<PokemonResponse> response = pokemonService.getPokemons(0, 100);
			assertNotNull(response);

			// Valid cache
			pokemonService.getPokemons(0, 100);
			verify(pokemonApiClient, times(1)).getPokemons(anyInt(), anyInt());
			
		} catch (IOException e) {
			fail("Error testGetPokemons!!");
		}
	}
	

	/**
	 * Get pokemon detail
	 */
	@Test
	void testGetPokemonDetail() {
		try {
			List<PokemonResponse> response = pokemonService.getPokemonDetail(2);
			assertNotNull(response);
		} catch (IOException e) {
			fail("Error testGetPokemonDetail!!");
		}
	}
	

	/**
	 * gest pokemon description
	 */
	@Test
	void testGetPokemonDescription() {
		String response = pokemonService.getPokemonDescription(1);
		assertNotNull(response);
	}

}
