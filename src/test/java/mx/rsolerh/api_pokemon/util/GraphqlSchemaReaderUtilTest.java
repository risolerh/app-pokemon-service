package mx.rsolerh.api_pokemon.util;

import static org.junit.jupiter.api.Assertions.*;

import java.io.IOException;

import org.junit.jupiter.api.Test;

/**
 * @author rsole
 *
 */
class GraphqlSchemaReaderUtilTest {

	@Test
	void testGetSchemaFromFileName() {
		try {
			String result = GraphqlSchemaReaderUtil.getSchemaFromFileName("pokemon_list");			
			assertNotNull(result);
		} catch (IOException e) {
			fail("file graphql not found !!");
		}
	}

}
