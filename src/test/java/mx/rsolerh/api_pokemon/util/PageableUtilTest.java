package mx.rsolerh.api_pokemon.util;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.concurrent.TimeUnit;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Timeout;

import mx.rsolerh.api_pokemon.model.PageableDto;

/**
 * @author rsole
 *
 */
class PageableUtilTest {

	@Test
	@Timeout(value = 1, unit = TimeUnit.MILLISECONDS)
	void testCreate() {		
		PageableDto pageable = PageableUtil.create(100, 3, 10);
		
		assertEquals(pageable.getIsNext(), false);
		assertEquals(pageable.getIsPrevious(), false);
		assertEquals(pageable.getOffset(), 30);
		assertEquals(pageable.getTotalPages(), 10);
	}

}
