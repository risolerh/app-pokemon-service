package mx.rsolerh.api_pokemon.controller;

import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import mx.rsolerh.api_pokemon.service.IPokemonService;

/**
 * @author rsole
 *
 */
@WebMvcTest(RestPokemonController.class)
class RestPokemonControllerTest {

	@Autowired
	private MockMvc mvc;

	@MockBean
	private IPokemonService pokemonService;
	

	/**
	 * @throws Exception
	 */
	@Test
	void testGetPokemonList() throws Exception {
		
		when(pokemonService.getPokemons(anyInt(), anyInt()))
		.thenReturn(new ArrayList<>());
		
		mvc.perform(MockMvcRequestBuilders
	  			.get("/api/pokemon")
	  			.accept(MediaType.APPLICATION_JSON)
	  			.contentType(MediaType.APPLICATION_JSON))
//	      .andDo(print())
	      .andExpect(status().isOk());
		
	}

	/**
	 * @throws Exception
	 */
	@Test
	void testGetPokemonDetailsById() throws Exception {
		
		when(pokemonService.getPokemonDetail(anyInt()))
		.thenReturn(new ArrayList<>());
		
		mvc.perform(MockMvcRequestBuilders
	  			.get("/api/pokemon/1")
	  			.accept(MediaType.APPLICATION_JSON)
	  			.contentType(MediaType.APPLICATION_JSON))
//	      .andDo(print())
	      .andExpect(status().isOk());
		
	}

}
